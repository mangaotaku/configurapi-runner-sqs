#!node

const commander = require('commander');
const App = require('./app');
const oneliner = require('one-liner');

///////////////////////////////////
// Utility functions
///////////////////////////////////
function format(prefix, str)
{
    let time = new Date().toISOString();
    return `${time} ${prefix}: ${oneliner(str)}`;
}
function logTrace(s)
{   
    console.log(format('Trace', s));
}
function logError(s)
{
    console.error(format('Error', s));
}
function logDebug(s)
{
    console.log(format('Debug', s));
}

///////////////////////////////////
// Process arguments
///////////////////////////////////
commander.option('-q, --queue [url]', 'SQS queue url for incoming messages')
         .option('-e, --error [url]', 'SQS queue url for error messages')
         .option('-r, --response [url]', 'SQS queue url for response messages')
         .option('-f, --file [path]', 'Path to the config file')
         .option('-c, --concurrency [number]', 'Concurrenncy level')
         .parse(process.argv);

let queueUrl   = commander.queue || process.env.CONFIGURAPI_SQS_REQUEST_QUEUE;
let configPath = commander.file ? commander.file : 'config.yaml';
let errorQueueUrl   = commander.error || process.env.CONFIGURAPI_SQS_ERROR_QUEUE;
let responseQueueUrl   = commander.response || process.env.CONFIGURAPI_SQS_RESPONSE_QUEUE;
let concurrency = commander.concurrency || process.env.CONFIGURAPI_SQS_CONCURRENCY_COUNT;

if(!queueUrl)
{
    commander.help();
    process.exit(1);
}

///////////////////////////////////
// Start the API
///////////////////////////////////
let app = new App();
app.on('error', (s) => {logError(s);});
app.on('debug', (s) => {logDebug(s);});
app.on('trace', (s) => {logTrace(s);});

process.on('SIGTERM', async () => {
    await app.stop();
    process.exit(0);
});

process.on('SIGINT', async () => {
    await app.stop();
    process.exit(0);
});

app.run({queueUrl: queueUrl, errorQueueUrl: errorQueueUrl, responseQueueUrl: responseQueueUrl, configPath:configPath, concurrencyCount: concurrency});