const SqsAdapter = require('./sqsAdapter');
const events = require('events');
const Configurapi = require('configurapi');
const SqsConsumer = require('sqs-consumer');

module.exports = class SqsRunner extends events.EventEmitter 
{
    constructor() 
    {
        super();

        this.queueUrl = undefined;
        this.errorQueueUrl = undefined;
        this.responseQueueUrl = undefined;
        this.concurrencyCount = 10;
        this.configPath = "config.yaml";
        this.service = undefined;
        this.config = {errorQueueUrl: undefined, responseQueueUrl: undefined};
        this.app = undefined;
        this.counter = 0;
    }

    run(options) 
    {
        this._handleOptions(options);

        let config = Configurapi.Config.load(this.configPath);

        this.service = new Configurapi.Service(config);
        this.service.on("trace", (s) => this.emit("trace", s));
        this.service.on("debug", (s) => this.emit("debug", s));
        this.service.on("error", (s) => this.emit("error", s));

        this.app = SqsConsumer.create({
                                        queueUrl: this.queueUrl,
                                        batchSize: this.concurrencyCount,
                                        handleMessage: async (message, done) => {
                                                this.counter++;
                                                try
                                                {
                                                    await this._requestListener(message);
                                                }
                                                finally
                                                {
                                                    this.counter--;
                                                }
                                                done();
                                        }
                                    });

        this.app.on('error', (err) => this.emit("error", err.message));
                                    
        this.app.start();
        
        this.emit("trace", "Server is listening...");
    }

    stop()
    {
        return new Promise(async (resolve, reject) => 
        {
            if(this.app)
            {   
                this.app.stop();

                const sleep = (ms) => new Promise((resolve2, reject2) => setTimeout(resolve2, ms));

                while(this.counter > 0) 
                {
                    this.emit("trace", `Server is stopping...[${this.counter}]`);
                    await sleep(500);
                }

                this.emit("trace", 'Server is stopped.');
                
                return resolve();
                
            } 
        });
    }

    _handleOptions(options) 
    {
        if (!options) return;

        if ('queueUrl' in options) this.queueUrl = options.queueUrl;
        if ('errorQueueUrl' in options) this.errorQueueUrl = options.errorQueueUrl;
        if ('responseQueueUrl' in options) this.responseQueueUrl = options.responseQueueUrl;
        if ('configPath' in options) this.configPath = options.configPath;
        if ('concurrencyCount' in options && options['concurrencyCount']) this.concurrencyCount = options.concurrencyCount;
        this.config = {errorQueueUrl: this.errorQueueUrl, responseQueueUrl: this.responseQueueUrl};
    }

    async _requestListener(incomingMessage) 
    {
        try {
            let event = new Configurapi.Event(await SqsAdapter.toRequest(incomingMessage));

            await this.service.process(event);
            
            await SqsAdapter.write(event.id, event.response, this.config);
        }
        catch (error) {
            let response = new Configurapi.ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

            try
            {
                await SqsAdapter.write('', response, this.config);
            }
            catch(err)
            {
                try
                {
                    this.emit("error", JSON.stringify(err));
                }
                catch(errorFromListner)
                {
                    console.log(errorFromListner);
                }
            }
        }
    }
};
